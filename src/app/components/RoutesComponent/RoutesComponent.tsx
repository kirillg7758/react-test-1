import React, { FC } from "react";
import { Redirect, Route, Switch } from "react-router-dom";

import MainPage from "../../pages/MainPage";
import PageNotFound from "../../pages/PageNotFound";

const RoutesComponent: FC = () => {

    return (
        <Switch>
            <Route exact path='/'>
                <MainPage />
            </Route>
            <Route exact path='/404'>
                <PageNotFound />
            </Route>
            <Redirect to='/404' />
        </Switch>
    )
}

export default RoutesComponent
