import React, { createContext, FC } from "react";

interface IContext {

}

export const context = createContext<IContext>({})

export const AppContext: FC<any> = ({children}) => {

    return (
        <context.Provider value={{}}>
            {children}
        </context.Provider>
    )
}
