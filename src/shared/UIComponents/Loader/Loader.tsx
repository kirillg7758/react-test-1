import React, { FC } from "react";
import { shallowEqual, useSelector } from "react-redux";

import { RootStore } from "../../../store/store";
import "./Loader.scss";

// interface ILoaderProps {
//
// }

const Loader: FC = () => {
    const { isLoading } = useSelector(({ loader }: RootStore) => loader, shallowEqual)
    const loaderShowStyle = isLoading
        ? { display: "flex" }
        : { display: "none" }
    return (
        <div className="loader" style={loaderShowStyle}>
            <div className="circle circle-1" />
            <div className="circle circle-2" />
            <div className="circle circle-3" />
            <div className="circle circle-4" />
            <div className="circle circle-5" />
        </div>
    )
}

export default Loader
