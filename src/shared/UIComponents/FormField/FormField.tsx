import React, { ChangeEvent, FC } from "react";
import { Field, FormikFormProps } from "formik";
import MaskedInput from "react-input-mask";

import "./FormField.scss";

interface IFormFieldProps extends FormikFormProps {
    label: string,
    message?: string,
    errorMessage?: string,
    fieldTouched?: boolean,
    mask?: string | (string | RegExp)[],
    handleChange?: (e: ChangeEvent<any>) => void,
    setFieldTouched?: (field: string, touch: boolean) => void
}

const FormField: FC<IFormFieldProps> = (props) => {
    const { label, name, message, errorMessage, fieldTouched, mask, handleChange, setFieldTouched, ...rest } = props
    const errorClass = (errorMessage && fieldTouched) && 'error'
    const errorMessageStyle = (errorMessage && fieldTouched) ? {
        transition: '.4s',
        opacity: 1
    } : {
        opacity: 0
    }
    return (
        <label htmlFor={label} className="form--label">
            {label}
            {mask?.length && <MaskedInput
                className={'form--label--field ' + errorClass}
                mask={mask}
                name={name}
                // maskPlaceholder={'_'}
                onChange={handleChange}
                onBlur={(e) => {
                    (setFieldTouched && name && errorMessage) && setFieldTouched(name, e.target.value.includes('_'));
                }}
            />}
            {!mask?.length && <Field
                id={label}
                className={'form--label--field ' + errorClass}
                name={name}
                {...rest}
            />}
            <span
                className="error--message"
                style={errorMessageStyle}
            >{errorMessage}</span>
            {message && <span className="form--label--message">{message}</span>}
        </label>
    )
}

export default FormField
