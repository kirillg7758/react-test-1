import React, { FC } from "react";
import { NavLink } from "react-router-dom";
import { Link } from "react-scroll";

interface ILink {
    key: number | string,
    name: string,
    to: string,
    link: boolean
}

interface ILinkItemProps {
    links: ILink[],
    className?: string,
    onClick?: (val?: boolean | string | number) => void
}

const LinkItem: FC<ILinkItemProps> = (props) => {
    const { links, className, onClick } = props
    return (
        <>
            {links.map((item) => {
                if (!item.link) {
                    return <Link
                        to={item.to}
                        className={"link " + (className || '')}
                        activeClass='active--link'
                        spy={true}
                        smooth={true}
                        offset={50}
                        duration={500}
                        key={item.key}
                        onClick={() => {
                            onClick && onClick()
                        }}
                    >{item.name}</Link>
                }
                return <NavLink
                    to={item.to}
                    key={item.key}
                    className={'link ' + (className || '')}
                    activeClassName='active--link'
                    onClick={() => {
                        onClick && onClick()
                    }}
                >{item.name}</NavLink>
            })}
        </>
    )
}

export default LinkItem
