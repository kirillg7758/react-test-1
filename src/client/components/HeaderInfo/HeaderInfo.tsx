import React, { FC } from "react";
import { Link } from "react-scroll";

import "./HeaderInfo.scss";

const HeaderInfo: FC = () => {

    return (
        <div className="header--info" id="about">
            <div className="container" style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start'
            }}>
                <h1 className="header--info--title heading-1">
                    TEST ASSIGNMENT
                    FOR FRONTEND DEVELOPER POSITION
                </h1>
                <div className="header--info__text">
                    <p className="header--info__text--item paragraph">
                        We kindly remind you that your test assignment should be submitted as a link to github/bitbucket repository. Please be patient, we consider and respond to every application that meets minimum requirements. We look forward to your submission. Good luck! The photo has to scale in the banner area on the different screens
                    </p>
                    <p className="header--info__text--item paragraph">
                        We kindly remind you that your test assignment should be submitted as a link to github/bitbucket repository.
                    </p>
                </div>
                <Link
                    to={'sign-up'}
                    className="btn"
                    spy={true}
                    smooth={true}
                    offset={50}
                    duration={500}
                >Sing up now</Link>
            </div>
        </div>
    )
}

export default HeaderInfo
