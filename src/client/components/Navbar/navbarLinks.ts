export const navbarLinks = [
    {
        key: 1,
        name: 'About me',
        to: 'about',
        link: false
    },
    {
        key: 2,
        name: 'Relationships',
        to: '/relationships',
        link: true
    },
    {
        key: 3,
        name: 'Requirements',
        to: '/requirements',
        link: true
    },
    {
        key: 4,
        name: 'Users',
        to: 'users',
        link: false
    },
    {
        key: 5,
        name: 'Sign Up',
        to: 'sign-up',
        link: false
    }
]
