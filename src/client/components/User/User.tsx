import React, { FC } from "react";
import ReactTooltip from "react-tooltip";

import { IUser } from "../../../store/reducers/userReducer";

import avatar from '../../../assets/icons/photo-cover.svg';

import "./User.scss";

interface IUserProps {
    user: IUser
}

const getShortNote = (note: string) => {
    return note.length > 22
        ? note
        .split('')
        .filter((item, idx) => idx + 1 < 22).join('') + '...'
        : note;
}

const User: FC<IUserProps> = (props) => {
    const { user } = props

    const email = getShortNote(user.email)
    const name = getShortNote(user.name)

    return (
        <div className="user">
            <img src={user.photo || avatar} alt="#" className="user--avatar" />
            <h3 data-for="name" data-tip={user.name} className="user--name">{name}</h3>
            <p className="user--description">{user.position}</p>
            <span data-for="email" data-tip={user.email} className="user--email">{email}</span>
            <span className="user--phone">{user.phone}</span>

            <ReactTooltip id="email" place="bottom" effect="float" />
            <ReactTooltip id="name" place="bottom" effect="float" />
        </div>
    )
}

export default User
