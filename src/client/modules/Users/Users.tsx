import React, { FC, useEffect, useState } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { v4 } from "uuid";

import Loader from "../../../shared/UIComponents/Loader";
import User from "../../components/User";
import { getUsersAction } from "../../../store/actions/getUsersAction";
import { RootStore } from "../../../store/store";

import "./Users.scss";

const Users: FC = () => {
    const { users, page, total_pages }  = useSelector(({ users }: RootStore) => users, shallowEqual)
    const [countUser, setCountUser] = useState<number>(window.innerWidth >= 540 ? 6 : 3)
    const dispatch = useDispatch()

    window.addEventListener('resize', (e: any) => {
        if (e.target.innerWidth >= 540) {
            countUser !== 6 && setCountUser(6)
        } else {
            countUser !== 3 && setCountUser(3)
        }
    }, false)

    useEffect(() => {
        dispatch(getUsersAction(page, countUser, true))
    }, [dispatch, page, countUser])

    return (
        <div className="users" id="users">
            <div className="container">
                <h2 className="users--title heading-1">Our cheerful users</h2>
                <h3 className="users--subtitle heading-2">Attention! Sorting users by registration date</h3>
                <div className="users__block">
                    {users.map((item) => {
                        return <User
                            user={item}
                            key={v4()}
                        />
                    })}
                </div>
                <Loader />
                {(total_pages !== page) && <button
                    type='button'
                    className="users--btn btn"
                    onClick={(e) => {
                        e.preventDefault()
                        dispatch(getUsersAction(page + 1, countUser)
                        )
                    }}
                >Show more</button>}
            </div>
        </div>
    )
}

export default Users
