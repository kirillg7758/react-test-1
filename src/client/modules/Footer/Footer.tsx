import React, { FC } from "react";

import "./Footer.scss";

const Footer: FC = () => {

    return (
        <footer className="footer">
            <h1 className="footer--title">© abz.agency specially for the test task</h1>
        </footer>
    )
}

export default Footer
