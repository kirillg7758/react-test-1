import React, { FC } from "react";
import { Link } from "react-scroll";

import infoImg from '../../../assets/img/man-laptop.svg';

import "./Info.scss";

const Info: FC = () => {

    return (
        <div className="info">
            <div className="container">
                <h2 className="info--title heading-1">Let's get acquainted</h2>
                <div className="info__block">
                    <img src={infoImg} alt="#" className="info__block--img"/>
                    <div className="info__block__description">
                        <h3 className="info__block__description--title">I am cool frontend developer</h3>
                        <p className="info__block__description--text">
                            We will evaluate how clean your approach to writing CSS and Javascript code is. You can use any CSS and Javascript 3rd party libraries without any restriction.
                        </p>
                        <p className="info__block__description--text">
                            If 3rd party css/javascript libraries are added to the project via bower/npm/yarn you will get bonus points. If you use any task runner (gulp/webpack) you will get bonus points as well. Slice service directory page P​SD mockup​ into HTML5/CSS3.
                        </p>
                        <Link
                            to={'sign-up'}
                            className="btn--flat"
                            spy={true}
                            smooth={true}
                            offset={50}
                            duration={500}
                        >Sing up now</Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Info
