import { Dispatch } from "redux";

import { axiosConfig } from "../axiosConfig";
import { IModalAction, ModalConstants } from "../reducers/modalReducer";
import { IUserActionsTypes, UserConstants } from "../reducers/userReducer";
import { ILoadingAction, LoadingConstants } from "../reducers/loadingReducer";

export const addUserAction = (FormData: FormData, token: string) => async (dispatch: Dispatch<IUserActionsTypes | IModalAction | ILoadingAction>) => {
    dispatch({
        type: LoadingConstants.OPEN_LOADING,
        payload: true
    })
    try {
        const { data } = await axiosConfig.post('/users', FormData, {
            headers: { 'Token': token }
        })

        const user = await axiosConfig.get(`/users/${data.user_id}`)
        dispatch({
            type: UserConstants.ADD_USER,
            payload: {
                user: user.data.user
            }
        })
        dispatch({
            type: ModalConstants.USE_MODAL,
            payload: {
                isOpen: true,
                message: data.message,
                success: data.success
            }
        })
    } catch (e) {
        dispatch({
            type: ModalConstants.USE_MODAL,
            payload: {
                isOpen: true,
                message: e.message,
                success: false
            }
        })
    }
    dispatch({
        type: LoadingConstants.CLOSE_LOADING,
        payload: false
    })
}
