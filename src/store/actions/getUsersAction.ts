import { Dispatch } from "react";
import { axiosConfig } from "../axiosConfig";
import { IStateUser, IUserActionsTypes, UserConstants } from "../reducers/userReducer";
import { ILoadingAction, LoadingConstants } from "../reducers/loadingReducer";

export const getUsersAction = (page: number, count: number, clear?: boolean) => async (dispatch: Dispatch<IUserActionsTypes | ILoadingAction>) => {
    dispatch({
        type: LoadingConstants.OPEN_LOADING,
        payload: true
    })
    try {
        const { data }: { data: IStateUser } = await axiosConfig.get(`/users?page=${page}&count=${count}`)
        dispatch({
            type: UserConstants.GET_USERS,
            payload: {
                data,
                clear: clear || false
            }
        })
    } catch (e) {

    }
    dispatch({
        type: LoadingConstants.CLOSE_LOADING,
        payload: false
    })
}
