import { Dispatch } from "redux";

import { IPositionAction, PositionConstants } from "../reducers/positionReducer";
import { axiosConfig } from "../axiosConfig";
import { IModalAction, ModalConstants } from "../reducers/modalReducer";

export const getPositionAction = () => async (dispatch: Dispatch<IPositionAction | IModalAction>) => {
    try {
        const { data } = await axiosConfig.get("/positions")
        dispatch({
            type: PositionConstants.GET_POSITIONS,
            payload: {
                positions: data.positions
            }
        })
    } catch (e) {
        dispatch({
            type: ModalConstants.USE_MODAL,
            payload: {
                isOpen: true,
                success: false,
                message: e.message
            }
        })
    }
}
