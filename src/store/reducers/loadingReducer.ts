export enum LoadingConstants {
    OPEN_LOADING = 'OPEN_LOADING',
    CLOSE_LOADING = 'CLOSE_LOADING'
}

export type ILoadingAction = {
    type: LoadingConstants.OPEN_LOADING,
    payload: true
} | {
    type: LoadingConstants.CLOSE_LOADING,
    payload: false
}

export interface ILoadingState {
    isLoading: boolean
}

const initialState: ILoadingState = {
    isLoading: false
}

export const loadingReducer = (state: ILoadingState = initialState, action: ILoadingAction) => {
    switch (action.type) {
        case LoadingConstants.OPEN_LOADING:
            return {
                isLoading: action.payload
            }
        case LoadingConstants.CLOSE_LOADING:
            return {
                isLoading: action.payload
            }
        default:
            return state
    }
}
