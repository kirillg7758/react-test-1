export enum ModalConstants {
    GET_TOKEN = 'GET_TOKEN',
    USE_MODAL = 'USE_MODAL',
    CLOSE_MODAL = 'CLOSE_MODAL'
}

export interface IModalState {
    token?: string,
    modal: {
        isOpen: boolean,
        success?: boolean,
        message?: string,
    }
}

export type IModalAction = {
    type: ModalConstants.GET_TOKEN,
    payload: {
        token: string
    }
} | {
    type: ModalConstants.USE_MODAL,
    payload: {
        isOpen: boolean,
        success: boolean,
        message: string,
    }
} | {
    type: ModalConstants.CLOSE_MODAL
}

const initialState: IModalState = {
    modal: {
        isOpen: false
    }
}

export const modalReducer = (state: IModalState = initialState, action: IModalAction) => {
    switch (action.type) {
        case ModalConstants.GET_TOKEN:
            return {
                ...state,
                token: action.payload.token
            }
        case ModalConstants.USE_MODAL:
            return {
                ...state,
                modal: {
                    isOpen: action.payload.isOpen,
                    success: action.payload.success,
                    message: action.payload.message
                }
            }
        case ModalConstants.CLOSE_MODAL:
            return {
                ...state,
                modal: {
                    ...state.modal,
                    isOpen: false
                }
            }
        default:
            return state
    }
}
