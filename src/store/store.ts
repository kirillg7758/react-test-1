import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from 'redux-thunk';

import { userReducer } from "./reducers/userReducer";
import { modalReducer } from "./reducers/modalReducer";
import { loadingReducer } from "./reducers/loadingReducer";
import { positionReducer } from "./reducers/positionReducer";

const rootReducer = combineReducers({
    users: userReducer,
    modal: modalReducer,
    loader: loadingReducer,
    userPosition: positionReducer
})

export const store = createStore(rootReducer, applyMiddleware(thunk))

export type RootStore = ReturnType<typeof rootReducer>
